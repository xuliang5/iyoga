#include <windows.h>
#include <iostream>
#include "mysql.h"
#include <string>
#include <iomanip>
#include <fstream>
#include <ctime>

using namespace std;


#ifndef test
//#define test
#endif

#ifndef debugXul
//#define debugXul
#endif

/*
 *成功返回0，失败返回非零
 *
 */
int getDBInfoConfig(string &dbuser, string &dbpasswd, string &dbip, string &dbname)
{
	ifstream fcin("../config.cfg");
	string line;
	
	try
	{
		getline(fcin, line);
		dbuser = line.substr(9, line.size() - 8);

		getline(fcin, line);
		dbpasswd = line.substr(11, line.size() - 10);

		getline(fcin, line);
		dbip = line.substr(7, line.size() - 6);

		getline(fcin, line);
		dbname = line.substr(9, line.size() - 8);
	}
	catch(exception)
	{
		return 1;
	}

	return 0;
}

/*
 *成功返回0，失败返回非零
 *
 */
MYSQL *connectDB(string dbuser, string dbpasswd, string dbip, string dbname)
{
	MYSQL *con = mysql_init((MYSQL*) 0); 

	if ( con !=NULL && mysql_real_connect(con, dbip.c_str(), dbuser.c_str(), dbpasswd.c_str(), dbname.c_str(), 3306, NULL, 0) ) 
	{

		if (!mysql_select_db(con,dbname.c_str())) 
		{ 
			con ->reconnect = 1;	
			char *query = "set names \'GBK\'";
			mysql_real_query(con,query,strlen(query));
		}
	}
	else
	{
		return NULL;
	}
	return con;
}

/*
 *获得新加入的member的ID，ID从1开始
 */
int getCurMemID(MYSQL *con)
{
	string sql = "select ID from member order by ID desc";

	mysql_real_query(con, sql.c_str(), sql.size());
	MYSQL_RES *res = mysql_store_result(con);
	if (res == NULL)
	{
		return 1;
	}
	MYSQL_ROW row = mysql_fetch_row(res);
#ifdef debugXul
	cout << row <<sql<<endl;
#endif
	return row == NULL ? 1 : atoi(row[0]) + 1;
}

string getStrFromInt(int CurID)
{
#ifdef debugXul
	cout << CurID <<endl;
#endif
	char strCurID[20];
	itoa(CurID, strCurID, 10);

	string id(strCurID);

	return id;
}
/*
 * 失败返回0，成功返回id
 *
 */
int addMember(MYSQL *con, string name, string address, string job)
{
	int CurID = getCurMemID(con);
	string strCurID = getStrFromInt(CurID);
#ifdef debugXul
	cout << CurID <<endl;
#endif
	string sql = "insert into member values(" + strCurID + ", '" + name + "', '" + address + "', '" + job + "')";

#ifdef debugXul
	cout << sql.c_str() <<endl;
#endif	
	int res = mysql_real_query(con, sql.c_str(), sql.size());

	return res ? 0 : CurID;
}

/*
 * 失败返回0，成功返回id
 *
 */
int getIDFromName(MYSQL *con, string name)
{
	string sql = "select ID from member where NAME = '" + name + "'";
	mysql_real_query(con, sql.c_str(), sql.size());
	MYSQL_ROW row = mysql_fetch_row(mysql_store_result(con));

	return row == NULL ? 0 : atoi(row[0]);
}

/*
 * 失败返回0，成功返回id
 *
 */
int addYearlyCard(MYSQL *con, string name, string startTime, string endTime)
{
	int CurID = getIDFromName(con, name);
	if (!CurID)
	{
		return 0;
	}

	string sql = "insert into yearlyCard values(" + getStrFromInt(CurID) + ", '" + startTime + "', '" + endTime + "')";
	
	int res = mysql_real_query(con, sql.c_str(), sql.size());

	return res ? 0 : CurID;
}

/*
 * 失败返回0，成功返回id
 *
 */
int addMeasuredCard(MYSQL *con, string name, int remainCnt, string startTime, string endTime)
{
	int CurID = getIDFromName(con, name);
#ifdef debugXul
	cout<<"CurID"<<CurID<<endl;
#endif
	if (!CurID)
	{
		return 0;
	}

	string strCurID = getStrFromInt(CurID);
	
	string sql = "insert into measuredCard values(" + strCurID + ", '" + startTime + "', '" + endTime + "', " + getStrFromInt(remainCnt) + ")";
#ifdef debugXul
	cout<<"sql"<<sql<<endl;
#endif
	int res = mysql_real_query(con, sql.c_str(), sql.size());

	return res ? 0 : CurID;
}


void writeLog(ofstream &fcoutLog, string log)
{
	fcoutLog << log << endl << flush;
}


int main()
{
#ifdef test
	time_t timer; 
	time(&timer); 
	tm* t_tm = localtime(&timer); 

	cout<<"today is "<<t_tm->tm_year+1900<<" "<<t_tm->tm_mon+1<<" "<<t_tm->tm_wday<<endl;
	cout<<t_tm->tm_hour<<" "<<t_tm->tm_min << " "<<t_tm->tm_sec<<endl;
	
	system("pause");
	return 0;
#endif


	ofstream fcoutLog("../log", ios::app);
	time_t timer; 
	time(&timer); 
	tm* t_tm = localtime(&timer); 


	string dbuser, dbpasswd, dbip, dbname;

	int res = getDBInfoConfig(dbuser, dbpasswd, dbip, dbname);

#ifdef debugXul
	cout << dbuser << dbpasswd << dbip << dbname <<endl;
#endif

	if (res)
	{
		cout << "configure error" <<endl;
		writeLog(fcoutLog, "configure error");
		return 0;
	}

	MYSQL *con = connectDB(dbuser, dbpasswd, dbip, dbname);
	if (con == NULL)
	{
		cout << "connect db error" <<endl;
		writeLog(fcoutLog, "connect db error");
		return 0;
	}
	else
	{
		cout << "connect db successful" <<endl;
		writeLog(fcoutLog, "connect db successful");
	}

	string iyoga, addOperator;
	
	while (1)
	{
		cin >> iyoga;
		if (iyoga == "q")
		{
			fcoutLog.close();
			break;
		}

		if (iyoga != "iyoga")
		{
			getline(cin, iyoga);
			cout << "input error" << endl;
			writeLog(fcoutLog, iyoga + " input error");
			continue;
		}

		cin >> addOperator;
		if (addOperator == "add-member")
		{
			string name, address, job;
			cin >> name >> address >> job;

			int res = addMember(con, name, address, job);
			if (!res)
			{
				cout << "add-member fail" << endl;
				writeLog(fcoutLog, "iyoga add-member fail");
				getline(cin, iyoga);
				continue;
			}
			else
			{
				cout << "OK." << name << "'s member id is " << setw(3) << setfill('0') << res << endl;
				writeLog(fcoutLog, "OK." + name + "'s member id is " + getStrFromInt(res));
			}
		}
		else if (addOperator == "add-card")
		{
			string name, cardName;
			cin >> name >> cardName;

			string startTime = getStrFromInt(t_tm->tm_year+1900) + "-" + getStrFromInt(t_tm->tm_mon+1) + "-" + getStrFromInt(t_tm->tm_mday)
				+ " " + getStrFromInt(t_tm->tm_hour) + ":"+ getStrFromInt(t_tm->tm_min) + ":"+ getStrFromInt(t_tm->tm_sec);

			if (cardName == "年卡")
			{
				string endTime = getStrFromInt(t_tm->tm_year+1900+1) + "-" + getStrFromInt(t_tm->tm_mon+1) + "-" + getStrFromInt(t_tm->tm_mday)
					+ " " + getStrFromInt(t_tm->tm_hour) + ":"+ getStrFromInt(t_tm->tm_min) + ":"+ getStrFromInt(t_tm->tm_sec);
				
				int res = addYearlyCard(con, name, startTime, endTime);
				if (!res)
				{
					cout << "addYearlyCard fail" << endl;
					writeLog(fcoutLog, "iyoga addYearlyCard fail");
					getline(cin, iyoga);
					continue;
				}
				else
				{
					cout << "OK. A new 年卡 added to " << name << "( "<< setw(3) << setfill('0') << res << ")" << endl;
					writeLog(fcoutLog, "OK. A new 年卡 added to " + name + "( " + getStrFromInt(res));
				}
			}
			else if (cardName == "次卡")
			{
				string endTime = getStrFromInt(t_tm->tm_year+1900+3) + "-" + getStrFromInt(t_tm->tm_mon+1) + "-" + getStrFromInt(t_tm->tm_mday)
					+ " " + getStrFromInt(t_tm->tm_hour) + ":"+ getStrFromInt(t_tm->tm_min) + ":"+ getStrFromInt(t_tm->tm_sec);

				int remainCnt;
				cin >> remainCnt;
				
				int res = addMeasuredCard(con, name, remainCnt, startTime, endTime);
				if (!res)
				{
					cout << "addMeasuredCard fail" << endl;
					writeLog(fcoutLog, "iyoga addMeasuredCard fail");
					getline(cin, iyoga);
					continue;
				}
				else
				{
					cout << "OK. A new 次卡 with "<< remainCnt << " times added to " << name << endl;
					writeLog(fcoutLog, "OK. A new 次卡 with "+ getStrFromInt(remainCnt) + " times added to " + name);
				}
			}
			else
			{
				getline(cin, iyoga);
				cout << "input error" << endl;
				writeLog(fcoutLog, "iyoga " + addOperator + " input error");
				continue;
			}
		}
		else
		{
			getline(cin, iyoga);
			cout << "input error" << endl;
			writeLog(fcoutLog, iyoga + " input error");
			continue;
		}
	}


	return 0;
}
