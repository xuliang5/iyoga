/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 12                       */
/* Created on:     2016/8/12 21:55:37                           */
/*==============================================================*/


drop table if exists measuredCard;

drop index if exists member.IDX_MEMBER_NAME;

drop table if exists member;

drop table if exists yearlyCard;

/*==============================================================*/
/* Table: measuredCard                                          */
/*==============================================================*/
create table measuredCard 
(
   ID                   integer                        null,
   STARTTIME            datetime                       null,
   ENDTIME              datetime                       null,
   REMAINCNT            integer                        null
);

comment on column measuredCard.ID is 
'会员编号';

comment on column measuredCard.STARTTIME is 
'办卡时间';

comment on column measuredCard.ENDTIME is 
'有效期结束时间';

comment on column measuredCard.REMAINCNT is 
'剩余次数';

/*==============================================================*/
/* Table: member                                                */
/*==============================================================*/
create table member 
(
   ID                   integer                        not null,
   NAME                 varchar(50)                    null,
   ADDRESS              varchar(50)                    null,
   JOB                  varchar(50)                    null,
   constraint PK_MEMBER primary key clustered (ID)
);

/*==============================================================*/
/* Index: IDX_MEMBER_NAME                                       */
/*==============================================================*/
create unique index IDX_MEMBER_NAME on member (
NAME ASC
);

/*==============================================================*/
/* Table: yearlyCard                                            */
/*==============================================================*/
create table yearlyCard 
(
   ID                   integer                        null,
   STARTTIME            datetime                       null,
   ENDTIME              datetime                       null
);

comment on column yearlyCard.ID is 
'会员的ID';

comment on column yearlyCard.STARTTIME is 
'办卡时间';

comment on column yearlyCard.ENDTIME is 
'有效期结束时间';

